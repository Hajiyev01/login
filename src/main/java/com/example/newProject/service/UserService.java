package com.example.newProject.service;

import com.example.newProject.dto.LoginDto;
import com.example.newProject.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UserService {
    List<User> getAllUser();

    User getUserId(Long id);

    User addUser(User user);

    Long getUpdateUser(Long id, User user);

    void getDeleteById(long id);

    User login(LoginDto loginDto);
}
