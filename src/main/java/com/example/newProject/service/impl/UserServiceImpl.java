package com.example.newProject.service.impl;

import com.example.newProject.dto.LoginDto;
import com.example.newProject.entity.User;
import com.example.newProject.repository.UserRepository;
import com.example.newProject.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User getUserId(Long id) {
        return userRepository.findById(id).get();
    }


    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Long getUpdateUser(Long id, User user) {
        if(userRepository.findById(id).isPresent()){
            User userr = userRepository.findById(id).get();
            userr.setName(user.getName());
            userr.setSurname(user.getSurname());
            userr.setMail(user.getMail());
            userr.setPassword(user.getPassword());
            userRepository.save(userr);

        }
        else{
            throw new RuntimeException("User not faunded");
        }
        return id;
    }

    @Override
    public void getDeleteById(long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User login(LoginDto loginDto) {
       String mail = loginDto.getMail();
       String password= loginDto.getPassword();
       User user = userRepository.findByMail(mail);
       if (user == null){
           return null;
    }
       if (!user.getPassword().equals(password)){
           return null;
       }
       return user;
    }
}
