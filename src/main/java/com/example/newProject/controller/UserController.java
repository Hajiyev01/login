package com.example.newProject.controller;

import com.example.newProject.entity.User;
import com.example.newProject.service.UserService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<User> getAllUser() {
        return userService.getAllUser();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id) {
        return userService.getUserId(id);
    }

    @PostMapping("/add")
    public User postUser (@RequestBody User user){
        return userService.addUser(user);

    }

    @PutMapping("/{id}")
    public Long updateUser(@PathVariable Long id,@RequestBody User user){
        return userService.getUpdateUser(id,user);
    }
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable long id){
        userService.getDeleteById(id);
    }
}