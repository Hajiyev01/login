package com.example.newProject.controller;

import com.example.newProject.dto.LoginDto;
import com.example.newProject.entity.User;
import com.example.newProject.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class LoginController {

    private  final UserService userService;

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginDto loginDto){
       User user = userService.login(loginDto);
       if (user!=null){
           return ResponseEntity.ok("Login successfully:");
       }
       else {
           return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login failed:");
       }
    }


}
