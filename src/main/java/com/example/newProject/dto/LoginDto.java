package com.example.newProject.dto;

import lombok.Data;

@Data
public class LoginDto {

    String mail;
    String password;
}
